# Projet_BS2_CB_CL

Master 2 Bioinformatique - Parcours IBI \
Cécile Beust (pseudo git cecilebeust) \
Corentin Lucas (pseudo git CoLucas)

Build protein interaction graph in Python using interaction text files 

* *main.py* : file containing all the python functions
* *test_functions.py* : file containing unitary test functions
